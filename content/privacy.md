+++
title = "Privacy Policy"
+++

Published on 2020-09-11
  
## Cookies

This website does not store cookies of any kind.
  
## Server logs

Server logs are only viewed in case of crashes or bugs and are automatically deleted after three days. Server logs typically include IP addresses and the pages that are viewed.

## Analytics

No website analytics are collected.
  
## Donations

Donations made through Liberapay are handled by Stripe. We store no personal information related to the payment. Stripe has their own [Privacy&nbsp;Policy](https://stripe.com/en-nl/privacy) outlining how they process the data they obtain.
    
Recurring donations arranged through Liberapay can be paused or cancelled at any time.
  
## Trademarks

Key to Identity Foundation, Keyoxide and the Keyoxide logo are trademarks of the Key to Identity Foundation and copyright of the Key to Identity Foundation.