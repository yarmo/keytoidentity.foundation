+++
title = "Projects"
+++

## Keyoxide

![Keyoxide](/keyoxide-screenshot.png)

[Keyoxide](https://keyoxide.org) is a strong modern, secure and privacy-friendly platform to establish your decentralized online identity. It allows you to link accounts on various online services and platforms together, prove they belong to you and establish an online identity. This puts you, the internet citizen, in charge when it comes to defining who you are on the internet instead of large corporations.

It is [open source](https://codeberg.org/keyoxide/keyoxide-web) (hosted on [Codeberg.org](https://codeberg.org)) licensed under [AGPL-v3.0-or-later](https://codeberg.org/keyoxide/keyoxide-web/src/branch/dev/LICENSE).

It launched on [July 1st, 2020](https://blog.keyoxide.org/keyoxide-launch/).

The Keyoxide Android app launched in [November 11th, 2021](https://blog.keyoxide.org/now-on-android/).
    
## doip.js

[doip.js](https://js.doip.rocks) is a javascript library to enable Node.js projects and websites to run the same identity verification technology as Keyoxide.

It is [open source](https://codeberg.org/keyoxide/doipjs) (hosted on [Codeberg.org](https://codeberg.org)) licensed under  [Apache-2.0](https://codeberg.org/keyoxide/doipjs/src/branch/main/LICENSE).

It launched on [November 9th, 2020](https://blog.keyoxide.org/keyoxide-project-update-1/).
