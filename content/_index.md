+++
title = "Welcome!"

template = "index.html"
page_template = "page.html"
+++

We are a small not-for-profit foundation registered in the Netherlands. We advocate for individual independence in digital identity management and develop tools to enable the verification of those identities.

Your privacy is respected here, this website will not follow you and no cookies are stored. Read our [Privacy Policy](/privacy) for more details. 
  
## What we do

The foundation exists to:

- empower people to define their own online identity;
- help people gain independence from online corporations;
- educate those who live under Surveillance Capitalism;
- build inclusive tools and technologies accessible to all;
- build online communities for the people by the people.

We are currently working on [Keyoxide](/projects#keyoxide), a tool to interact with Decentralized OpenPGP Identity Proofs.
    
## Why we do this

Back in the early 2010's, I became fascinated by the "Login with your COMPANY X account" buttons scattered all over the internet. A single company providing a concept of identity, protecting it and increasing its value over time.

What could go wrong?

Following the many detailed reports on the data-greedy nature of companies like Google and Facebook and their impact on society and our lives, I started exploring novel ways to safeguard digital identity of the online citizenry while respecting privacy and user sovereignty.

This ambition resulted in the launch of [Keyoxide](https://keyoxide.org) and the founding of the [Key to Identity Foundation](/).

Read about our motivations in our [manifesto](/manifesto).
  
## The foundation

<div class="person">
    <img class="profile" src="/yarmo.jpg">
    <p>
        <b>Yarmo Mackenbach</b>
        <br>
        Founder
    </p>
    <p>
        <a href="https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d">keyoxide</a> | 
        <a href="https://yarmo.eu">website</a> | 
        <a href="https://fosstodon.org/@yarmo">fediverse</a>
    </p>
</div>
  
## History

<ul class="history">
    <li>
        <h3>July 2020</h3>
        <p>Launch of <a href="https://keyoxide.org">Keyoxide</a></p>
    </li>
    <li>
        <h3>October 2020</h3>
        <p>Founding of <a href="/">Key to Identity Foundation</a></p>
    </li>
    <li>
        <h3>November 2020</h3>
        <p>Funding through <a href="https://nlnet.nl/project/Keyoxide/">NLnet/NGI0 grant</a></p>
    </li>
    <li>
        <h3>November 2021</h3>
        <p>Release of Keyoxide Android app</p>
        <p>Launch of the <a href="https://ariadne.id">Ariadne Spec</a></p>
    </li>
    <li>
        <h3>August 2022</h3>
        <p>Funding through <a href="https://nlnet.nl/project/Keyoxide-signatures/">NLnet/NGI0 grant</a></p>
    </li>
</ul>