+++
title = "Manifesto"
+++

## The Internet is in **dire conditions**.

A handful of companies define who we are on the Internet. They asked us to trust them and when we did, they sold our personal data and became the most powerful entities the Human race has ever known.

## We live in the Age of **Surveillance Capitalism**.

The rich get rich not by building products we want or need, but by building products they make sure we cannot live without. They watch us as we consume then sell our personal data to the highest bidder.

## But we say **no more**.

No more abusing our data to undermine our humanity.  
No more abusing our data to sell us products when we are vulnerable.  
No more abusing our data to stir unrest among the people.  
No more abusing our data to decide elections and manipulate the balance of power.

## The next generation.

The next generation of technology is the one that **empowers the people**, **is fair to all** and **propels forward Humanity**.

We, existing on the Internet, have:

1. a right to privacy;
1. a right to ownership over our personal data;
1. a right to own and shape our online identity;
1. a duty to protect our fellow netizens from those seeking to prey on them;
1. the leverage collectively to take away the power from those seeking to abuse it for personal and/or financial gain.
