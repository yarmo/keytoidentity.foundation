+++
title = "Donate"
+++

**If your hopes for the future of the internet align with our ambitions, please help us work towards that future.**

Funds obtained through donations are used to cover server rental costs, development tools and outreach initiatives. Remaining funds cover monthly bills to sustain the open source developer life.

Donations are handled by [Liberapay](https://liberapay.com/Keyoxide/).

Buying me a [coffee](https://ko-fi.com/yarmo) is also truly appreciated :)